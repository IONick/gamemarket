var marketApp = angular.module('market', ['ngRoute']);

marketApp.config(function ($routeProvider) {
    $routeProvider.when('/products', {
        templateUrl: 'html-templates/products.html'
    });
});