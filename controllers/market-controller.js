marketApp.controller('MarketController', ['$scope', '$interval', function ($scope, $interval) {
    let timeend = new Date(2020, 1, 1);

    function setTimeToAction() {
        let today = new Date();
        today = Math.floor((timeend - today) / 1000);
        let tsec = today % 60;
        today = Math.floor(today / 60);
        if (tsec < 10) tsec = '0' + tsec;
        let tmin = today % 60;
        today = Math.floor(today / 60);
        if (tmin < 10) tmin = '0' + tmin;
        let thour = today % 24;
        today = Math.floor(today / 24);
        return today + " дней " + thour + " часов " + tmin + " минут " + tsec + " секунд";
    }

    $interval(function () {
        $scope.timeToAction = setTimeToAction();
    }, 1000);

    $scope.goToTreeComponent = function () {
        window.location.href = 'phone-tree-component/phone-tree.html';
    };

    $scope.showProducts = function () {
        window.location = '#!products';
    };
}]);