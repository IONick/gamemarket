application.controller('HomeController', ['$scope', 'PhoneService', function ($scope, phoneService) {
    $scope.phones = [
        {
            id: 1,
            name: 'test name 1',
            description: 'Test description 1',
            photoUrl: 'photoUrl 1',
            category: 'Категория 1'
        },
        {
            id: 2,
            name: 'test name 2',
            description: 'Test description 2',
            photoUrl: 'photoUrl 2',
            category: 'Категория 2'
        },
        {
            id: 3,
            name: 'test name 3',
            description: 'Test description 3',
            photoUrl: 'photoUrl 3',
            category: 'Категория 1'
        },
        {
            id: 4,
            name: 'test name 4',
            description: 'Test description 4',
            photoUrl: 'photoUrl 4',
            category: 'Категория 3'
        },
        {
            id: 5,
            name: 'test name 5',
            description: 'Test description 5',
            photoUrl: 'photoUrl 5',
            category: 'Категория 3'
        },
        {
            id: 5,
            name: 'test name 6',
            description: 'Test description 6',
            photoUrl: 'photoUrl 6',
            category: 'Категория 6'
        },
        {id: 5, name: 'test name 7', description: 'Test description 7', photoUrl: 'photoUrl 7', category: 'Категория 7'}
    ];

    phoneService.initPhones($scope);

    $scope.addPhone = function () {
        window.location = '#!phone/add';
    };

    $scope.addCategory = function () {
        window.location = '#!category';
    };

    $scope.sort = function () {
        window.location = '#!sortable';
    };

    $scope.select = function (selectedPhone) {
        $scope.phones.forEach(function (phone) {
            phone.selected = false;
        });

        selectedPhone.selected = true;
    };

    $scope.remove = function () {
        var phones = $scope.phones.filter(function (phone) {
            return !phone.selected;
        });

        phoneService.initPhones(phones);
        $scope.phones = phones;
    };

    $scope.edit = function () {
        var phones = phoneService.get().filter(function (phone) {
            return phone.selected;
        });

        if (phones[0]) {
            window.location = '#!phone/edit';
        }
    };

    $scope.goToMarket = function () {
        window.location.href = '../index.html';
    };

    $scope.treeOptions = {
        nodeChildren: "children",
        dirSelectable: true,
        injectClasses: {
            ul: "a1",
            li: "a2",
            liSelected: "a7",
            iExpanded: "a3",
            iCollapsed: "a4",
            iLeaf: "a5",
            label: "a6",
            labelSelected: "a8"
        }
    };

    $scope.initTree = function () {
        $scope.treeData = [];

        $scope.phones.forEach(function (phone) {
            var inserted = false;
            $scope.treeData.forEach(function (treeItem) {
                if (treeItem.name === phone.category) {
                    treeItem.children.push(phone);
                    inserted = true;
                }
            });

            if (!inserted) {
                $scope.treeData.push({name: phone.category, children: [phone]});
            }
        });
    };

    $scope.initTree();
}]);