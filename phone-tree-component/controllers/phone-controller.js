application.controller('PhoneController', ['$scope', 'PhoneService', function ($scope, phoneService) {
    var editPhone;
    $scope.canEdit = window.location.hash === '#!phone/edit';

    if ($scope.canEdit) {
        var phones = phoneService.get().filter(function (phone) {
            return phone.selected;
        });

        editPhone = phones[0];

        if (editPhone) {
            $scope.name = editPhone.name;
            $scope.description = editPhone.description;
            $scope.photoUrl = editPhone.photoUrl;
            $scope.category = editPhone.category;
        }
    }


    $scope.submit = function () {
        if ($scope.canEdit) {
            editPhone.name = $scope.name;
            editPhone.description = $scope.description;
            editPhone.photoUrl = $scope.photoUrl;
            editPhone.category = $scope.category;
        } else {
            phoneService.add({ name: $scope.name, description: $scope.description, photoUrl: $scope.photoUrl, category: $scope.category });
        }

        $scope.cancel();
    };

    $scope.cancel = function () {
        window.location = "#!";
    };
}]);
