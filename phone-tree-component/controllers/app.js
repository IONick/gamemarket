var application = angular.module('application', ['ngRoute']).config(function ($routeProvider) {

    let always = function ($q, $timeout) {
        $timeout(function () {
            $('.ui-publisher-dialog').addClass('show');
            $('.ui-overlay').css('height', '100%');
        }, 100);
    };

    $routeProvider.when('/phone/add', {
        templateUrl: 'views/phone.html',
        controller: 'PhoneController',
        resolve: { always: always }
    });

    $routeProvider.when('/phone/edit', {
        templateUrl: 'views/phone.html',
        controller: 'PhoneController',
        resolve: { always: always }
    });

    $routeProvider.when('/category', {
        templateUrl: 'views/category.html',
        controller: 'CategoryController',
        resolve: { always: always }
    });

    $routeProvider.when('/sortable', {
        templateUrl: 'views/sortable.html',
        controller: 'SortableController',
        resolve: { always: always }
    });

    $routeProvider.when('/phone/tree', {
        templateUrl: 'views/category.html',
        controller: 'CategoryController',
        resolve: { always: always }
    });
}).service('PhoneService', function () {
    let _$scope = null;

    this.initPhones = function ($scope) {
        _$scope = $scope;
    };

    this.add = function (phone) {
        phone.id = _$scope.phones.length + 1;
        _$scope.phones.push(phone);
        _$scope.initTree();
    };

    this.get = function () {
        return _$scope.phones;
    };
});

